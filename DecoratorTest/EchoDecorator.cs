﻿namespace DecoratorTest
{
    public class EchoDecorator : FallbackDecorator<IEcho>, IEcho
    {
        public EchoDecorator(IEcho primary, IEcho secondary)
            : base(primary, secondary)
        {
        }

        public string Echo(string toEcho)
        {
            return Invoke(() => _primary.Echo(toEcho), () => _secondary.Echo(toEcho));
        }

        public void MutedEcho(string toMute)
        {
            Invoke(() => _primary.MutedEcho(toMute), () => _secondary.MutedEcho(toMute));
        }
    }
}