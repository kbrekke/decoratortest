﻿namespace DecoratorTest
{
    public interface IEcho
    {
        string Echo(string toEcho);

        void MutedEcho(string toMute);
    }
}