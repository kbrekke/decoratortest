﻿namespace DecoratorTest
{
    public class TheEcho : IEcho
    {
        public string Echo(string toEcho)
        {
            return toEcho;
        }

        public void MutedEcho(string toMute)
        {
        }
    }
}