﻿using System;
using System.Collections.Concurrent;

namespace DecoratorTest
{
    public abstract class FallbackDecorator<TInterface>
        where TInterface : class
    {
        private static ConcurrentDictionary<int, bool> _notImplementedMethods = new ConcurrentDictionary<int, bool>();

        protected readonly TInterface _primary;
        protected readonly TInterface _secondary;

        protected FallbackDecorator(TInterface primary, TInterface secondary)
        {
            _primary = primary;
            _secondary = secondary;
        }

        protected T Invoke<T>(Func<T> primary, Func<T> secondary)
        {
            int hash = primary.Method.GetHashCode();

            if (_notImplementedMethods.ContainsKey(hash))
            {
                return secondary();
            }

            return TryInvoke(primary, secondary, hash);
        }

        protected void Invoke(Action primary, Action secondary)
        {
            int hash = primary.Method.GetHashCode();

            if (_notImplementedMethods.ContainsKey(hash))
            {
                secondary();
            }

            TryInvoke(primary, secondary, hash);
        }

        private static T TryInvoke<T>(Func<T> primary, Func<T> secondary, int hash)
        {
            T t = default(T);

            try
            {
                t = primary();
            }
            catch (NotImplementedException)
            {
                MarkMethodAsNotImplemented(hash);

                t = secondary();
            }

            return t;
        }

        private static void TryInvoke(Action primary, Action secondary, int hash)
        {
            try
            {
                primary();
            }
            catch (NotImplementedException)
            {
                MarkMethodAsNotImplemented(hash);

                secondary();
            }
        }

        private static void MarkMethodAsNotImplemented(int hash)
        {
            _notImplementedMethods.TryAdd(hash, true);
        }
    }
}