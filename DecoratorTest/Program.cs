﻿using System;

namespace DecoratorTest
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var decorator = new EchoDecorator(new TheReverseEcho(), new TheEcho());
            var decoratorTwo = new EchoDecorator(new TheReverseEcho(), new TheEcho());

            Console.WriteLine(decorator.Echo("Echo!"));
            Console.WriteLine(decoratorTwo.Echo("Echo!!"));
            decorator.MutedEcho("...");
            decoratorTwo.MutedEcho("....");

            Console.WriteLine();
            Console.WriteLine("Press a key to exit ...");
            Console.ReadKey();
        }
    }
}